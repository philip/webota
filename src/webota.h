#include <Arduino.h>
#include <ESP8266WebServer.h>

class WebOTA {
public:
    //! @brief Create OTA server on default port 80
    WebOTA();
    WebOTA(int port);

    //! @brief Needs to be ran in "setup" stage
    //! @param wiFiClient used to check for connection before initializing. A
    //!        WiFi connections needs to be established for the setup to succeed
    //! @return true if successful
    bool setup(ESP8266WiFiClass* wiFiClient);
    void run();

private:
    ESP8266WebServer _server;
    ESP8266WiFiClass* _wiFiClient;
};