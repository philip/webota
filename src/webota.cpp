#include "webota.h"
#include "WiFiUdp.h"

const char* serverIndex =
    "<form method='POST' action='/update' enctype='multipart/form-data'><input "
    "type='file' name='update'><input type='submit' value='Update'></form>";

WebOTA::WebOTA(int port)
: _server(port)
, _wiFiClient(nullptr)
{}

bool WebOTA::setup(ESP8266WiFiClass* wiFiClient)
{
    _wiFiClient = wiFiClient;

    if (_wiFiClient->status() == WL_CONNECTED)
    {
        _server.on("/", HTTP_GET, [&]() {
            _server.sendHeader("Connection", "close");
            _server.send(200, "text/html", serverIndex);
        });
        _server.on(
            "/update",
            HTTP_POST,
            [&]() {
                _server.sendHeader("Connection", "close");
                _server.send(
                    200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
                ESP.restart();
            },
            [&]() {
                HTTPUpload& upload = _server.upload();
                if (upload.status == UPLOAD_FILE_START)
                {
                    Serial.setDebugOutput(true);
                    WiFiUDP::stopAll();
                    Serial.printf("Update: %s\n", upload.filename.c_str());
                    uint32_t maxSketchSpace =
                        (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
                    if (!Update.begin(maxSketchSpace))
                    { // start with max available size
                        Update.printError(Serial);
                    }
                } else if (upload.status == UPLOAD_FILE_WRITE)
                {
                    if (Update.write(upload.buf, upload.currentSize) !=
                        upload.currentSize)
                    {
                        Update.printError(Serial);
                    }
                } else if (upload.status == UPLOAD_FILE_END)
                {
                    if (Update.end(true))
                    { // true to set the size to the current progress
                        Serial.printf("Update Success: %u\nRebooting...\n",
                                      upload.totalSize);
                    } else
                    {
                        Update.printError(Serial);
                    }
                    Serial.setDebugOutput(false);
                }
                yield();
            });
        _server.begin();
        return true;

    } else
    {
        Serial.println("WiFi Failed");
    }
    return false;
}

void WebOTA::run()
{
    _server.handleClient();
}